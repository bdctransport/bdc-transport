import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';



@Injectable({
  providedIn: 'root'
})
export class RequestHttpService {

  private onUrl = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o';
  private ltUrl = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l';
  private itUrl = 'http://www.poatransporte.com.br/php/facades/process.php?a=il&p=';


  constructor(private http: HttpClient) { }

  getOnIn(type: number): Observable<any> {
    const url = type === 1 ? this.onUrl : this.ltUrl;
    return this.http.get(url);
  }

  findIt(id: number): Observable<any> {
    return this.http.get(`${this.itUrl}${id}`);
  }

}
