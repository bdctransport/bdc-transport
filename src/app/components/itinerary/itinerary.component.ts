import {
  Component,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
  ComponentFactory
} from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { RequestHttpService } from '../../services/request-http.service';
import { MapaComponent } from '../../components/mapa/mapa.component';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'app-itinerary',
  templateUrl: './itinerary.component.html',
  styleUrls: ['./itinerary.component.css']
})
export class ItineraryComponent {
  // values used in the class
  valuesTransports: Array<object> = [];
  valuesPagination: Array<object> = [];
  activePage = 1;
  componentRef: any;


  @ViewChild('maps', { read: ViewContainerRef , static: true }) viewContainerRef: ViewContainerRef;

  constructor(
    private notifiyService: NotificationService,
    private requestHttpService: RequestHttpService,
    private resolver: ComponentFactoryResolver,
  ) {
  }

  // created maps
  createComponent(valuesTransports) {
    this.viewContainerRef.clear();
    const factory = this.resolver.resolveComponentFactory(MapaComponent);
    const componentRef = this.viewContainerRef.createComponent(factory);
    componentRef.instance.showMap(valuesTransports);
  }

  destroyComponent() {
    this.componentRef.destroy();
  }


  // settings pagination
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.valuesPagination = this.valuesTransports.slice(startItem, endItem);

  }


  // function search transport itinerary
  transportIti(value) {

    // notifications
    this.notifiyService.messageLoad();
    this.valuesPagination = [];
    this.valuesTransports = [];

    const promise = new Promise((resolve, reject) => {
      this.requestHttpService.findIt(value.id)
        .toPromise()
        .then(
          response => {
            if (response) {
              for (const [key, data] of Object.entries(response)) {
                if (typeof data === 'object') {
                  this.valuesTransports.push(data);
                }
              }
              // maps
              this.createComponent(this.valuesTransports);

              // star values for view
              this.notifiyService.clear();
              this.valuesPagination = this.valuesTransports.slice(0, 10);
              this.activePage = 1;

              resolve(true);

            } else {
              this.notifiyService.showError('Erro no servidor,favor tente novamente!');
              resolve(false);

            }
            resolve(true);
          },
          msg => {
            this.notifiyService.showError('Erro no servidor, favor tente novamente!');
            reject(false);
          }
        );
    });

    return promise;

  }

}
