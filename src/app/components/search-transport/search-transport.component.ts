import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { TransportListComponent } from '../../components/transport-list/transport-list.component';

@Component({
  selector: 'app-search-transport',
  templateUrl: './search-transport.component.html',
  styleUrls: ['./search-transport.component.css']
})
export class SearchTransportComponent implements OnInit {


  @ViewChild(TransportListComponent, { static: true })
  transportList: TransportListComponent;


  transports = {
    valueTransport: { num: 0, name: '' }
  };

  linesTransports: Array<object> = [];
  optionsTransports: Array<object> = [
    { num: 1, name: 'Ônibus' },
    { num: 2, name: 'Lotação' }
  ];

  ngOnInit(): void {
  }

  constructor(private notifiyService: NotificationService) {}

  searchTransport() {
    const valueTransport = this.transports.valueTransport.num;
    if (valueTransport) {
      this.transportList.transportValue(valueTransport);
    } else {
      this.notifiyService.showWarning('Por favor, selecione um meio de transporte!');
    }
  }
}
