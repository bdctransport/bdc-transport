import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTransportComponent } from './search-transport.component';

describe('SearchTransportComponent', () => {
  let component: SearchTransportComponent;
  let fixture: ComponentFixture<SearchTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
