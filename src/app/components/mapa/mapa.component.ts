import { Component , Input } from '@angular/core';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent {
  // google maps zoom level
  zoom = 10;
  // initial center position for the map
  lat = -30.00868521100000000;
  lng = -51.14298116400000000;
  @Input() markers = [];

  showMap(localization) {
    this.markers = [];
    let index = 0;
    const interval = setInterval(() => {
      if (localization[index]) {
        this.markers.push(localization[index]);
        index++;
      }
      if (index >= localization.length - 1) {
        clearInterval(interval);
      }
    }, 600);


  }
}


