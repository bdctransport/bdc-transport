import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { NotificationService } from '../../services/notification.service';
import { RequestHttpService } from '../../services/request-http.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ItineraryComponent } from '../../components/itinerary/itinerary.component';


@Component({
  selector: 'app-transport-list',
  templateUrl: './transport-list.component.html',
  styleUrls: ['./transport-list.component.css']
})

export class TransportListComponent implements OnInit {

  @ViewChild('modal', { static: false }) childModal: ModalDirective;


  @ViewChild(ItineraryComponent, { static: true })
  itinerary: ItineraryComponent;

  // values used in the class
  valuesTransports: Array<object> = [];
  valuesPagination: Array<object> = [];
  activePage = 1;
  it = {codigo: 0 , nome : ''};

  constructor(public element: ElementRef,
              private notifiyService: NotificationService,
              private requestHttpService: RequestHttpService) {
  }

  ngOnInit() { }


  // settings pagination
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.valuesPagination = this.valuesTransports.slice(startItem, endItem);

  }

  showModal(value) {

    this.it = {codigo: value.codigo , nome : value.nome};
    this.itinerary.transportIti(value).then(returnPromos => {
      if (returnPromos) {
        this.childModal.show();
      }
    });
  }

  hideModal(): void {
    this.childModal.hide();
  }

  // function search transport
  transportValue(value: number) {

    // notifications
    this.notifiyService.messageLoad();
    this.valuesPagination = [];
    this.valuesTransports = [];

    // get values
    this.requestHttpService.getOnIn(value)
      .subscribe(
        (response) => {
          if (response) {
            // star values for view
            this.notifiyService.clear();
            this.valuesTransports = response;
            this.valuesPagination = this.valuesTransports.slice(0, 10);
            this.activePage = 1;

            // animation page
            setTimeout(() => {
              this.element.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
            }, 1000);

          } else {
            this.notifiyService.showError('Erro no servidor,favor tente novamente!');
          }
        },
        (error) => {
          this.notifiyService.showError('Erro no servidor, favor tente novamente!');
        }
      );
  }

}
