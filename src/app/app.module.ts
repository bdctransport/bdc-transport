import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { RequestHttpService } from './services/request-http.service';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { SearchTransportComponent } from './components/search-transport/search-transport.component';
import { HeaderComponent } from './components/header/header.component';
import { TransportListComponent } from './components/transport-list/transport-list.component';
import { PaginationModule } from 'ngx-bootstrap';
import { PipesModule } from 'w-ng5';
import { ItineraryComponent } from './components/itinerary/itinerary.component';
import { ModalModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { MapaComponent } from './components/mapa/mapa.component';



@NgModule({
  declarations: [
    AppComponent,
    SearchTransportComponent,
    HeaderComponent,
    TransportListComponent,
    ItineraryComponent,
    MapaComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBZRYkssoZI-WALHwTMEFkmAI44JinnShE'
    }),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-full-width',
      preventDuplicates: false,
    }),
    HttpClientModule,
    PaginationModule.forRoot(),
    PipesModule
  ],
  providers: [RequestHttpService],
  bootstrap: [AppComponent],
  entryComponents: [MapaComponent]
})
export class AppModule { }
